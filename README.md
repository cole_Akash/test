<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>


# TO-do List 
## System Requirements

<li>PHP (Version 7.0+)</li>
<li>Composer</li>
<li>Git</li>

# Framework used
Laravel Framework 7.8.1

# Project Setup for local
Git clone the repository
Run composer install to load PHP dependencies to root of project folder

```bash
composer install
```

# After Clone 
create 

```bash
 .env 
```  
file and start modifying.

# Thank You!!! 





