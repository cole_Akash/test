<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Database\Eloquent\Model;

class UserController extends Controller
{
    public function uploadAvatar(Request $request )
        {
            if ($request->hasFile('image')) {

                User::uploadAvatar($request->image);


                //$request->session()->flash('message', 'image successfully uploaded ');
                return redirect()->back()->with('message', 'image uploaded'); //success

            }
            // $request->session()->flash('error', 'image not  uploaded ');
             return redirect()->back()->with('error', 'image not uploaded'); //error

        }



         public function index(){



        //we can define the route here and can call the function in route

        $data = [
            'name' => 'akash',
            'email' => 'akash@gmail.com',
            'password' =>  'password',
        ];

        $user  = User::all();
        return $user;

        return view('home');
    }
}
