@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>

                {{-- upload images --}}
                <div class="card-body">
                   {{-- include('layouts.flash') --}}

                   <x-alert>
                     <p>here is response from image upload</p>
                   </x-alert>

                    <form action="/upload" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="image" id="">
                        <input type="submit" value="Upload" id="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
